"""
URL configuration for brain_two project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from todos.views import (
    TodoLists,
    TodoDetails,
    create_todo,
    edit_todo,
    delete_todo,
    create_todo_item,
)

urlpatterns = [
    path("admin/", admin.site.urls),
    path("todos/", TodoLists, name="todo_list_list"),
    path("todos/<int:id>", TodoDetails, name="todo_list_detail"),
    path("todos/create", create_todo, name="create_todo"),
    path("todos/edit/<int:id>", edit_todo, name="todo_list_update"),
    path("todos/delete/<int:id>", delete_todo, name="todo_list_delete"),
    path("todos/createItem/", create_todo_item, name="create_item"),
]
