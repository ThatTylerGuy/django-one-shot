from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList
from todos.forms import TodoForm, TodoItemForm


# Create your views here.
def TodoLists(request):
    todo_lists = TodoList.objects.all()
    context = {"TodoList_list": todo_lists}
    return render(request, "todos/todo.html", context)


def TodoDetails(request, id):
    todo_itmes = get_object_or_404(TodoList, id=id)
    context = {"TodoList_object": todo_itmes}
    return render(request, "todos/todoDetails.html", context)


def create_todo(request):
    if request.method == "POST":
        form = TodoForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("todo_list_list")
    else:
        form = TodoForm()
    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)


def edit_todo(request, id):
    todoList = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoForm(request.POST, instance=todoList)
        if form.is_valid():
            form.save()
            return redirect("todo_list_list")
    else:
        form = TodoForm(instance=todoList)
    context = {
        "todolist": todoList,
        "form": form,
    }
    return render(request, "todos/edit.html", context)


def delete_todo(request, id):
    model_instance = TodoList.objects.get(id=id)
    if request.method == "POST":
        model_instance.delete()
        return redirect("todo_list_list")

    return render(request, "todos/delete.html")


def create_todo_item(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("todo_list_list")
    else:
        form = TodoItemForm()
    context = {
        "form": form,
    }
    return render(request, "todos/createItem.html", context)
