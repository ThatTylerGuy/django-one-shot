from django.urls import path
from todos.views import (
    TodoLists,
    TodoDetails,
    create_todo,
    edit_todo,
    delete_todo,
    create_todo_item,
)

urlpatterns = [
    path("todos/", TodoLists, name="todo_list_list"),
    path("todos/<int:id>", TodoDetails, name="todo_list_detail"),
    path("todos/create", create_todo, name="create_todo"),
    path("todos/edit/<int:id>", edit_todo, name="todo_list_update"),
    path("todos/delete/<int:id>", delete_todo, name="todo_list_delete"),
    path("todos/createItem", create_todo_item, name="create_item"),
]
